---
title: "Android Applications"
date: 2020-04-12T11:52:41+02:00
draft: false
tags: ["android"]
categories: []
---

|                                                                                     |                                                                                    |
| ----------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------- |
| [NewPipe](https://newpipe.schabi.org/)                                              | An inutitive, feature-rich and privacy friendly app for watching videos on YouTube |
| [TrebleShot](https://github.com/genonbeta/TrebleShot)                               | Allows you to send and receive files without an internet connection                |
| [F-Droid](https://f-droid.org/)                                                     | Applications for the Android platform                                              |
| [Office](https://www.collaboraoffice.com/solutions/collabora-office-android-ios/)   | Edit documents directly on your phone or tablet                                    |
| [OsmAnd](https://osmand.net/)                                                       | Global Mobile Map Viewing and Navigation                                           |
| [Caffeinate](https://play.google.com/store/apps/details?id=xyz.omnicron.caffeinate) | Will keep your screen awake                                                        |
| [Book Reader](https://gitlab.com/axet/android-book-reader)                          | Android book reader application                                                    |
| [Blokada](https://blokada.org/)                                                     | Ad blocker                                                                         |