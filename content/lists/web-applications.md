---
title: "Web Applications"
date: 2020-04-11T21:50:03+02:00
draft: false
tags: ["web"]
categories: []
---

|                                       |                                   |
| ------------------------------------- | --------------------------------- |
| [AnonAddy](https://anonaddy.com/)     | Anonymous Email Forwarding        |
| [BorgBase](https://www.borgbase.com/) | Simple and Secure Offsite Backups |
