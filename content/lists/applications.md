---
title: "Applications"
date: 2020-04-11T22:12:15+02:00
draft: false
tags: ["apps"]
categories: []
---

|                                                      |                                                                         |
| ---------------------------------------------------- | ----------------------------------------------------------------------- |
| [Standard Notes](https://standardnotes.org/)         | A free, open-source, and completely encrypted notes app.                |
| [Jami](https://jami.net/)                            | Video calling                                                           |
| [Signal](https://www.signal.org/)                    | Instant messaging                                                       |
| [Privacy Badger](www.eff.org/privacybadger)          | Browser extension that automatically learns to block invisible trackers |
| [uBlock Origin](https://getublock.com/)              | Blocker browser extension                                               |
| [GitAhead](https://gitahead.github.io/gitahead.com/) | Graphical Git client                                                    |
| [GIMP](https://www.gimp.org/)                        | Image editor                                                            |
| [Inkscape](https://inkscape.org/)                    | Vector image editor                                                     |
| [Scribus](https://www.scribus.net/)                  | Desktop publishing                                                      |
| [Syncthing](https://syncthing.net/)                  | Synchronize folders                                                     |
| [LibreOffice](https://www.libreoffice.org/)          | Free office suite                                                       |