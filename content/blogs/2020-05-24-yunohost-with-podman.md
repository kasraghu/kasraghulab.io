---
title: Hosting Yunohost
date: 2020-05-24T16:32:17.695Z
draft: false
tags:
  - hosting
  - container
  - vm
---
Yunohost seems to be the easy way to run multiple server applications.

I would like to use Fedora Silverblue as it gives a stable base. Silverblue is made to be used for containers.Podman wouldn't even start the container as it was unable bind ports.

But I couldn't get the DNS of container working in Docker. There seems to be a problem with installing `resolvconf` package. After being unsuccessful in _resolv_ ing this issue, I tried using the normal Fedora workstation as host. Same issues still existed. Maybe Yunohost container needs a Debian host ?

Finally, chose to create a debian VM with [virt-manager](https://virt-manager.org/). Couldn't figure out the network settings to use NAT, but using a bridge network between VM and host worked. With bridge mode, I cannot SSH in to VM from host, but everything else works.