---
title: "Backup Files to Local Drive"
date: 2020-04-11T14:16:39+02:00
draft: false
tags: ["syncthing","rsync","backup","fedora","systemd"]
---

I want to backup all the photos from smartphones to an external HDD, automatically. I have been using [syncthing](https://syncthing.net/) for this (`Receive Only` folder on PC with `ignoreDelete` option). The external HDD is connected to a PC with [Fedora Silverblue](https://silverblue.fedoraproject.org/) on it. The OS mounts the HDD to a file path linked to the user that is logged in. So Syncthing does not work properly when there are multiple user accounts (The file path of HDD changes) or when no one is logged in (The HDD is not mounted). 

As a workaround I have created a folder in `/home/` with read & write access to all the users of PC. This folder is used by Syncthing in usual `Send & Receive` mode. [rsync](https://rsync.samba.org/) is used to send files from there to HDD. This way from time to time I am able delete all the pictures from PC and smartphone and as they are saved on external HDD. 

To run rsync regularly using Systemd:

cameraarchive.service
```
[Unit]
Description=Update CameraArchive

[Service]
Type=simple
ExecStart=rsync -avzh /home/sharedfolder/Camera/ /run/media/user/hdd/CameraArchive
```

cameraarchive.timer
```
[Unit]
Description=CameraArchive update timer

[Timer]
OnCalendar=daily
Persistent=true

[Install]
WantedBy=timers.target
```
Place the above files in `~/.config/systemd/user/` and in terminal do:
```
$ systemctl daemon-reload
$ systemctl --user --now enable cameraarchive.timer
```

With the above configuration the service runs before the HDD is mounted, unless the system is ON during midnight. So replaced `OnCalendar=daily` with `OnStartupSec=30min`. This way the service is run 30 minutes after login. 