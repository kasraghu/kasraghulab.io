---
title: Netlify Widget
date: 2020-04-14T07:58:21.452Z
draft: false
tags:
  - pages
  - website
  - hosting
  - cms
---
First the scripts required for Netlify identity service are placed in the website source. Then I realised all the viewers do not need these extra scripts. So now I use the Netlify script injection feature in the UI. It would have been better to use the File-based configuration. That way I have all the configuration in the source and easy to move the CMS hosting to any [netlifycms](https://www.netlifycms.org) based service. But at the moment I did not find the option in their [documentation](https://docs.netlify.com/configure-builds/file-based-configuration/#post-processing).