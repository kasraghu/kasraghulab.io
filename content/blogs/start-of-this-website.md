---
title: Start of This Website
date: 2020-04-11T09:45:59.000Z
draft: false
tags:
  - dat
  - pages
  - website
  - hosting
---
I wanted to make this website using [dat protocol](https://dat.foundation/), [beaker browser](https://beakerbrowser.com/) and [hashbase](https://hashbase.io/). At the moment one limiting factor I see is that, it does not seem trivial to edit the website on multiple devices while keeping the same dat URL. I will try to make this site available on dat anyway.

This website is hosted on [gitlab pages](https://kasraghu.gitlab.io/) and made using [Hugo](https://gohugo.io/). Netlify is also setup for easy uploads and editing.

Next step: https://indieweb.org/ 

Anyway I hope to add content to this site continuously.